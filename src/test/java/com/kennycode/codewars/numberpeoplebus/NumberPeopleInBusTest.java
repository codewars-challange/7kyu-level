package com.kennycode.codewars.numberpeoplebus;

import com.kennycode.codewars.NumberPeopleInBus;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class NumberPeopleInBusTest {

    private NumberPeopleInBus numberPeopleInBus;

    @Before
    public void setUp() throws Exception {
        numberPeopleInBus = new NumberPeopleInBus();
    }

    @Test
    public void sidTest(){

        List<int[]> peopleInBus = new ArrayList<int[]>();
        peopleInBus.add(new int[]{10,0});
        peopleInBus.add(new int[]{3,5});
        peopleInBus.add(new int[]{2,5});
        numberPeopleInBus.numberOfPeopleRemain(peopleInBus);

    }
}